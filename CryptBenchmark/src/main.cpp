//============================================================================
// Name        : imxCryptBenchmark.cpp
// Author      : Marat Minshin
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <stdio.h>
#include <iostream>

#include <enginesound/crypt/CryptSahara.h>
#include <enginesound/crypt/CryptLinux.h>

#include <enginesound/statistic/TimeLatency.h>

#include <enginesound/Log.h>

using namespace std;
using namespace enginesound;

static unsigned char key[] =
{ 0x38, 0x43, 0xB8, 0x35, 0xB7, 0xA7, 0x88, 0x8F, 0xE0, 0x92, 0x67, 0x23, 0xD4, 0xB6, 0x64, 0xA4 };

static unsigned char data16[] =
{ 0x24, 0xfb, 0x5f, 0x05, 0x8d, 0x5c, 0x06, 0xf9, 0x1d, 0x8b, 0xde, 0xa9, 0x0b, 0x05, 0x1b, 0x75 };

static unsigned char data16enc[] =
{ 0x99, 0x8f, 0x6f, 0x72, 0xc0, 0x41, 0x84, 0xdb, 0x62, 0xc3, 0x4b, 0xd0, 0x3d, 0x26, 0xee, 0x3c };

static unsigned char data128[] =
		{ 0x87, 0x1d, 0x1b, 0xfc, 0x86, 0x36, 0x97, 0x95, 0x5f, 0x4b, 0x2f, 0x1e, 0x27, 0xba, 0x56, 0xb3, 0x62, 0x6d,
				0x9c, 0xc5, 0x90, 0xa2, 0x29, 0x82, 0xca, 0x63, 0x08, 0x24, 0xb4, 0xd6, 0x4f, 0xc2, 0xef, 0x96, 0x4a,
				0xa8, 0xc4, 0xd9, 0xc0, 0xa2, 0x57, 0x16, 0x51, 0x3f, 0xc7, 0xa4, 0x57, 0x0f, 0xf5, 0x57, 0x5b, 0x8e,
				0x4b, 0x50, 0x2b, 0x9e, 0xe3, 0x56, 0x32, 0x9a, 0x4f, 0xf1, 0x43, 0x0e, 0xf2, 0xb8, 0xa3, 0x52, 0x19,
				0x70, 0x9f, 0xbc, 0xd0, 0x8f, 0xd0, 0xd7, 0x77, 0x64, 0xaf, 0x7c, 0xac, 0x9c, 0x00, 0xbe, 0x3b, 0x94,
				0xee, 0x41, 0x0a, 0x11, 0x63, 0x73, 0x2e, 0x11, 0x54, 0x2c, 0x97, 0x64, 0x43, 0xf1, 0x58, 0x9c, 0xa3,
				0x53, 0xaa, 0x16, 0x02, 0x9b, 0x6e, 0xbe, 0x7a, 0xad, 0xee, 0xad, 0xea, 0xa4, 0x56, 0x2c, 0xe6, 0x68,
				0xa3, 0x13, 0x8c, 0x86, 0x83, 0x9b, 0x15, 0x2c };
static unsigned char data128enc[] =
		{ 0x83, 0x5a, 0x58e, 0x57e, 0x46, 0x67, 0x3f, 0xd7, 0x9e, 0xc6, 0xcb, 0xeb, 0xb6, 0x66, 0x24, 0x08, 0x62, 0x9f,
				0xc2, 0x24, 0x78, 0xda, 0xf5, 0x8d, 0x71, 0x09, 0x21, 0x19, 0x04, 0x81, 0x9c, 0x29, 0xcc, 0x82, 0xeb,
				0xa9, 0xe9, 0x6a, 0x3d, 0x11, 0xaa, 0x09, 0x0c, 0x1c, 0xa4, 0x3e, 0x5e, 0xea, 0x73, 0x5f, 0x59, 0x88,
				0xd8, 0xf8, 0x61, 0xc0, 0x75, 0x09, 0x32, 0x88, 0x49, 0x65, 0xca, 0xc7, 0x54, 0xb8, 0xba, 0x27, 0x34,
				0x1b, 0xbc, 0xe2, 0xa7, 0x76, 0xe5, 0xc9, 0x3d, 0xf2, 0xbf, 0x38, 0x57, 0x5b, 0x8a, 0xf5, 0x84, 0x0a,
				0x21, 0xd8, 0x87, 0x23, 0x4b, 0xb7, 0x0e, 0x55, 0x4f, 0x7a, 0x57, 0x52, 0x7e, 0x79, 0xb8, 0xa2, 0x81,
				0x26, 0xa0, 0xfa, 0xa2, 0x67, 0x73, 0x10, 0x6f, 0x7d, 0x58, 0x3b, 0x91, 0xe8, 0x6c, 0x46, 0x75, 0xde,
				0xae, 0x7d, 0x37, 0x9e, 0xe5, 0xeb, 0x5b, 0x3c };

const size_t sizeBuf = 1 * 1024 * 1024;

bool testCrypt(const char *name, const char *namedevice, CryptBase::SessionCrypt::Ptr crypt, unsigned char * data,
		unsigned char * enc, const size_t size)
{
	if (!crypt)
	{
		Log::error("Crypt %s not create", namedevice);
		return false;
	}

	unsigned char * result = new unsigned char[size];

	TimeLatency latencyCrypt;
	latencyCrypt.beginMeasure();
	crypt->crypt(result, data, size);
	latencyCrypt.endMeasure();

	if (memcmp(result, enc, size) != 0)
		cout << "crypt " << name << ", on " << namedevice << " - failed" << endl;
	else
		cout << "crypt " << name << ", on " << namedevice << "  - success" << endl;

	cout << "      encrypt " << namedevice << ", size: " << size << ", time: " << latencyCrypt << endl;

	delete[] result;

	return true;
}

bool testStepCrypt(const char *name, const char *namedevice, CryptBase::SessionCrypt::Ptr crypt, unsigned char * data,
		unsigned char * enc, const size_t size, const size_t step)
{
	if (!crypt)
	{
		Log::error("Crypt %s not create", namedevice);
		return false;
	}

	unsigned char * result = new unsigned char[size];

	TimeLatency latencyCrypt;
	latencyCrypt.beginMeasure();
	for (size_t pr = 0; pr < size; pr += step)
		crypt->crypt(result + pr, data + pr, step);
	latencyCrypt.endMeasure();

	if (memcmp(result, enc, size) != 0)
		cout << "step crypt " << name << ", on " << namedevice << " - failed" << endl;
	else
		cout << "step crypt " << name << ", on " << namedevice << "  - success" << endl;

	cout << "      step encrypt " << namedevice << ", size: " << size << ", time: " << latencyCrypt << endl;

	delete[] result;

	return true;
}

bool testDecrypt(const char *name, const char *namedevice, CryptBase::SessionDecrypt::Ptr decrypt, unsigned char * data,
		unsigned char * dataenc, const size_t size)
{
	if (!decrypt)
	{
		Log::error("Crypt %s not create", namedevice);
		return false;
	}

	unsigned char * result = new unsigned char[size];

	TimeLatency latencyCrypt;
	latencyCrypt.beginMeasure();
	decrypt->decrypt(result, dataenc, size);
	latencyCrypt.endMeasure();

	if (memcmp(result, data, size) != 0)
		cout << "decrypt " << name << ", on " << namedevice << " - failed" << endl;
	else
		cout << "decrypt " << name << ", on " << namedevice << "  - success" << endl;

	cout << "      dencrypt " << namedevice << ", size: " << size << ", time: " << latencyCrypt << endl;

	delete[] result;

	return true;
}

bool testStepDecrypt(const char *name, const char *namedevice, CryptBase::SessionDecrypt::Ptr decrypt,
		unsigned char * data, unsigned char * dataenc, const size_t size, const size_t step)
{
	if (!decrypt)
	{
		Log::error("Crypt %s not create", namedevice);
		return false;
	}

	unsigned char * result = new unsigned char[size];

	TimeLatency latencyCrypt;
	latencyCrypt.beginMeasure();
	for (size_t pr = 0; pr < size; pr += step)
		decrypt->decrypt(result + pr, dataenc + pr, step);
	latencyCrypt.endMeasure();

	if (memcmp(result, data, size) != 0)
		cout << "step decrypt " << name << ", on " << namedevice << " - failed" << endl;
	else
		cout << "step decrypt " << name << ", on " << namedevice << "  - success" << endl;

	cout << "      step dencrypt " << namedevice << ", size: " << size << ", time: " << latencyCrypt << endl;

	delete[] result;

	return true;
}

int main()
{
#ifdef SAHARA
	CryptSahara cryptSahara;
	if (!cryptSahara.init())
		Log::error(cryptSahara.getLastErrorMsg().c_str());

	testCrypt("16", "FSL SHW", cryptSahara.createCrypt(Crypt::AES128, Crypt::CBC, key, sizeof(key)), data16, data16enc,
			sizeof(data16));
	testCrypt("128", "FSL SHW", cryptSahara.createCrypt(Crypt::AES128, Crypt::CBC, key, sizeof(key)), data128,
			data128enc, sizeof(data128));
	testDecrypt("16", "FSL SHW", cryptSahara.createDecrypt(Crypt::AES128, Crypt::CBC, key, sizeof(key)), data16,
			data16enc, sizeof(data16));
	testDecrypt("128", "FSL SHW", cryptSahara.createDecrypt(Crypt::AES128, Crypt::CBC, key, sizeof(key)), data128,
			data128enc, sizeof(data128));

	testStepCrypt("128", "FSL SHW", cryptSahara.createCrypt(Crypt::AES128, Crypt::CBC, key, sizeof(key)), data128,
			data128enc, sizeof(data128), 32);
	testStepDecrypt("128", "FSL SHW", cryptSahara.createDecrypt(Crypt::AES128, Crypt::CBC, key, sizeof(key)), data128,
			data128enc, sizeof(data128), 32);
#endif /* SAHARA */

#ifdef DEVCRYPTOLINUX
	CryptLinux cryptLinux;
	if (!cryptLinux.init())
		Log::error(cryptLinux.getLastErrorMsg().c_str());

	testCrypt("16", "/dev/crypto", cryptLinux.createCrypt(Crypt::AES128, Crypt::CBC, key, sizeof(key)), data16,
			data16enc, sizeof(data16));
	testCrypt("128", "/dev/crypto", cryptLinux.createCrypt(Crypt::AES128, Crypt::CBC, key, sizeof(key)), data128,
			data128enc, sizeof(data128));
	testDecrypt("16", "/dev/crypto", cryptLinux.createDecrypt(Crypt::AES128, Crypt::CBC, key, sizeof(key)), data16,
			data16enc, sizeof(data16));
	testDecrypt("128", "/dev/crypto", cryptLinux.createDecrypt(Crypt::AES128, Crypt::CBC, key, sizeof(key)), data128,
			data128enc, sizeof(data128));

	testStepCrypt("128", "/dev/crypto", cryptLinux.createCrypt(Crypt::AES128, Crypt::CBC, key, sizeof(key)), data128,
			data128enc, sizeof(data128), 32);
	testStepDecrypt("128", "/dev/crypto", cryptLinux.createDecrypt(Crypt::AES128, Crypt::CBC, key, sizeof(key)),
			data128, data128enc, sizeof(data128), 32);

#endif /* DEVCRYPTOLINUX */

	return 0;
}
