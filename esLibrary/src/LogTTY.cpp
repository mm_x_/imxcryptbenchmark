/*
 * LogTTY.cpp
 *
 *  Created on: 21.11.2011
 *      Author: user
 */

#include <enginesound/log/LogTTY.h>

using namespace boost;
using namespace boost::asio;

namespace enginesound
{

#ifdef WIN32
	const char * LogTTY::defaultPath = "COM0";
#else
	const char * LogTTY::defaultPath = "/dev/tty0";
#endif

LogTTY::LogTTY(const char * filename, const size_t speed, const size_t minLevel) :
		LogBase(minLevel), m_pService(), m_pPort()
{
	init(filename, speed);
}

LogTTY::~LogTTY()
{
	close();
}

bool LogTTY::init(const char * filename, const size_t baud)
{
	try
	{
		m_pService = new io_service();
		m_pPort = new serial_port(*m_pService, filename);

		serial_port_base::baud_rate BAUD(baud);
		m_pPort->set_option(BAUD);
	} catch (std::exception& e)
	{
		setErrorMsg(e.what());
		return false;
	}

	return true;
}

void LogTTY::close()
{
	if (m_pPort)
	{
		m_pPort->cancel();
		m_pPort->close();
	}

	delete m_pPort;
	delete m_pService;
}

void LogTTY::log(const size_t level, const char * msg)
{
	if(!is_ok())
		return;

	LogBase::log(level, msg);
	char * buff = new char[strlen(msg) + 7];

	sprintf(buff, "%lu: %s\r\n", std::min(level, size_t(255)), msg);
	m_pPort->write_some(asio::buffer(buff, strlen(buff)));

	delete [] buff;
}

} /* namespace enginesound */
