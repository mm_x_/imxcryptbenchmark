/*
 * LogFile.cpp
 *
 *  Created on: 27.01.2010
 *      Author: user
 */

#include "enginesound/log/LogFile.h"

using namespace std;

namespace enginesound
{

const char * LogFile::defaultPath = "/var/log/esPlayer.log";

LogFile::LogFile(const char * filename, const size_t minLevel) :
		LogBase(minLevel), m_logFile()
{
	init(filename);
}

LogFile::~LogFile()
{
	close();
}

bool LogFile::init(const char * filename)
{
	m_logFile.open(filename);

	if (m_logFile.is_open())
		return true;
	else
	{
		setErrorMsg("Can`t open log file %s", filename);
		return false;
	}
}

void LogFile::close()
{
	m_logFile.close();
}

}
