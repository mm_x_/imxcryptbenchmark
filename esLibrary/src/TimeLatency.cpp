/*
 * TimeLatency.cpp
 *
 *  Created on: 27.09.2010
 *      Author: user
 */

#include "enginesound/statistic/TimeLatency.h"

namespace enginesound
{

std::ostream & operator<<(std::ostream &stream, const TimeLatency & obj)
{
	if(obj.isMeasure())
	{
		stream << "min: " << obj.getMin().toFloat() * 1000 << ' ';
		stream << "max: " << obj.getMax().toFloat() * 1000 << ' ';
		stream << "avg: " << obj.getAvg().toFloat() * 1000;
	}
	else
		stream << "not measured";

	return stream;
}

}
