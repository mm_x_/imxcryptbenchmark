/*
 * CryptSahara.cpp
 *
 *  Created on: 01.11.2012
 *      Author: u4
 */


#include <enginesound/crypt/CryptSahara.h>
#include <assert.h>

#ifdef SAHARA

#include <enginesound/Log.h>

#include <apihelp.h>

using namespace std::tr1;

namespace enginesound
{

CryptSahara::CryptSahara() :
		m_cap(NULL), m_ctx(NULL)
{
}

CryptSahara::~CryptSahara()
{
	close();

	delete m_ctx;
	m_ctx = NULL;
}

bool CryptSahara::init()
{
	fsl_shw_return_t code;

	m_ctx = new fsl_shw_uco_t();

	/* Set Results Pool size to ??? */
	fsl_shw_uco_init(m_ctx, 256);

	/* Tell hw API that we are here */
	code = fsl_shw_register_user(m_ctx);
	if (code != FSL_RETURN_OK_S)
	{
		Log::error("fsl_shw_register_user() failed with error: %s", fsl_error_string(code));
		delete m_ctx;
		m_ctx = NULL;

		return false;
	}

	m_cap = fsl_shw_get_capabilities(m_ctx);
	if (m_cap == NULL)
	{
		Log::error("fsl shw not supported!");
		close();
		return false;
	}

	if (true)
		fsl_shw_uco_set_flags(m_ctx, FSL_UCO_BLOCKING_MODE);
	else
		fsl_shw_uco_clear_flags(m_ctx, FSL_UCO_BLOCKING_MODE);

	return true;
}

bool CryptSahara::close()
{
	if (!m_ctx)
	{
		Log::error("fsl_shw not init");
		return false;
	}

	fsl_shw_return_t code;

	code = fsl_shw_deregister_user(m_ctx);
	if (code != FSL_RETURN_OK_S)
	{
		Log::error("fsl_shw_deregister_user() failed with error: %s", fsl_error_string(code));
		return false;
	}

	delete m_ctx;
	m_ctx = NULL;

	return true;
}

CryptBase::SessionCrypt::Ptr CryptSahara::createCrypt(const Algorithm alg, const Mode mode, const unsigned char* key,
		const size_t keysize)
{
	if (!m_ctx || !m_cap)
		setErrorMsg("fsl shw not init");

	SessionCrypt * crypt = new SessionCrypt(m_ctx, m_cap, toFSL(alg), toFSL(mode), key, keysize);
	return SessionCrypt::Ptr(crypt);

}

CryptBase::SessionDecrypt::Ptr CryptSahara::createDecrypt(const Algorithm alg, const Mode mode,
		const unsigned char* key, const size_t keysize)
{
	if (!m_ctx || !m_cap)
		setErrorMsg("fsl shw not init");

	return SessionDecrypt::Ptr(new SessionDecrypt(m_ctx, m_cap, toFSL(alg), toFSL(mode), key, keysize));
}

fsl_shw_key_alg_t CryptSahara::toFSL(const Algorithm type)
{
	switch (type)
	{
	case AES128:
		return FSL_KEY_ALG_AES;
	case AES256:
		return FSL_KEY_ALG_AES;
	case DES:
		return FSL_KEY_ALG_DES;
	case TDES:
		return FSL_KEY_ALG_TDES;
	default:
		return fsl_shw_key_alg_t(-1);
	}
}
fsl_shw_sym_mode_t CryptSahara::toFSL(const Mode mode)
{
	switch (mode)
	{
	case CBC:
		return FSL_SYM_MODE_CBC;
		break;
	case ECB:
		return FSL_SYM_MODE_ECB;
		break;
	default:
		return fsl_shw_sym_mode_t(-1);
	}
}

fsl_shw_sko_t* CryptSahara::getSko(fsl_shw_uco_t* ctx, fsl_shw_pco_t *cap, const fsl_shw_key_alg_t alg,
		const fsl_shw_sym_mode_t m, const unsigned char* data, const size_t size)
{
	fsl_shw_return_t code;

	int ignore_des_key_parity = 1;

	if (alg == fsl_shw_key_alg_t(-1) || m == fsl_shw_sym_mode_t(-1))
		return NULL;

	if (!fsl_shw_pco_check_sym_supported(cap, alg, m)){
	Log::error("Crypt algorithm(mode) not supported");
	return NULL;
}

	fsl_shw_sko_t* keyInfo = (fsl_shw_sko_t *) malloc(sizeof(*keyInfo));

	/* Initialize crypto objects */
	if (!fsl_shw_pco_check_pk_supported(m_cap))
	{
		fsl_shw_sko_init(keyInfo, alg);
		/* Insert the key into the key object */
		fsl_shw_sko_set_key(keyInfo, data, size);
	}
	else
	{
		/* Have dryice. Operation is different */
		/* fsl_shw_sko_init_pf_key(key_info, algorithm, FSL_SHW_PF_KEY_PRG); */
		(keyInfo)->algorithm = fsl_shw_key_alg_t(-1);
		(keyInfo)->flags = -1;
		(keyInfo)->keystore = NULL;

		fsl_shw_sko_set_key_length(keyInfo, size);
		code = fsl_shw_establish_key(ctx, keyInfo, FSL_KEY_WRAP_ACCEPT, (const uint8_t*) data);
		if (code != FSL_RETURN_OK_S)
		{
			Log::error("Could not establish PRG key: %s", fsl_error_string(code));
			free(keyInfo);
			return NULL;
		}

	}

	/* Set DES 'ignore parity' for certain cases. */
	if (ignore_des_key_parity && ((alg == FSL_KEY_ALG_DES) || (alg == FSL_KEY_ALG_TDES)))
	{
		fsl_shw_sko_set_flags(keyInfo, FSL_SKO_KEY_IGNORE_PARITY);
	}

	/* Set up context */
	if (m == FSL_SYM_MODE_CBC)
	{
		fsl_shw_scco_set_flags(ctx, FSL_SYM_CTX_INIT);
	}

	return keyInfo;
}

bool CryptSahara::closeSko(fsl_shw_uco_t* ctx, fsl_shw_sko_t* key)
{
	bool retVal = true;
	fsl_shw_return_t code;

	if (key)
	{
		/* Need to release the key */
		code = fsl_shw_release_key(ctx, key);
		if (code != FSL_RETURN_OK_S)
		{
			Log::error("Unable to release the established key: %s", fsl_error_string(code));
			retVal = false;
		}
	}

	free(key);

	return true;
}

fsl_shw_scco_t* CryptSahara::getSymCtx(fsl_shw_uco_t* ctx, const fsl_shw_key_alg_t type, const fsl_shw_sym_mode_t mode)
{
	fsl_shw_scco_t *symCtx = (fsl_shw_scco_t *) malloc(sizeof(*symCtx));
	memset(symCtx, 0, sizeof(*symCtx));
	fsl_shw_scco_init(symCtx, type, mode);

	return symCtx;
}

void CryptSahara::freeSymCtx(fsl_shw_scco_t* ctx)
{
	free(ctx);
}

CryptSahara::SessionCrypt::SessionCrypt(fsl_shw_uco_t* ctx, fsl_shw_pco_t *cap, const fsl_shw_key_alg_t type,
		const fsl_shw_sym_mode_t mode, const unsigned char* data, const size_t size) :
		m_ctx(ctx), m_symCtx(NULL)
{
	if (!ctx || !cap)
	{
		setErrorMsg("fsl shw not init");
		return;
	}

	m_shwKey = CryptSahara::getSko(ctx, cap, type, mode, data, size);
	if (m_shwKey == NULL)
		return;

	m_symCtx = CryptSahara::getSymCtx(ctx, type, mode);
	if (m_symCtx == NULL)
		return;
}

CryptSahara::SessionCrypt::~SessionCrypt()
{
	CryptSahara::closeSko(m_ctx, m_shwKey);
	CryptSahara::freeSymCtx(m_symCtx);
}

bool CryptSahara::SessionCrypt::crypt(unsigned char *result, const unsigned char * input, const size_t size,
		CallBackBase::Ptr callback)
{
	fsl_shw_return_t code;

	code = fsl_shw_symmetric_encrypt(m_ctx, m_shwKey, m_symCtx, size, input, result);
	if (code != FSL_RETURN_OK_S)
	{
		setErrorMsg("Could not encrypt: %s\n", fsl_error_string(code));
		return false;
	}

	if (size >= 16)
		memcpy(m_symCtx->context, result + size - 16, 16);

	if (callback)
		(*callback)();

	return true;
}

CryptSahara::SessionDecrypt::SessionDecrypt(fsl_shw_uco_t* ctx, fsl_shw_pco_t *cap, const fsl_shw_key_alg_t type,
		const fsl_shw_sym_mode_t mode, const unsigned char* data, const size_t size) :
		m_ctx(ctx), m_symCtx(NULL)
{
	if (!ctx || !cap)
	{
		setErrorMsg("fsl shw not init");
		return;
	}

	m_shwKey = CryptSahara::getSko(ctx, cap, type, mode, data, size);
	if (m_shwKey == NULL)
		return;

	m_symCtx = CryptSahara::getSymCtx(ctx, type, mode);
	if (m_symCtx == NULL)
		return;
}

CryptSahara::SessionDecrypt::~SessionDecrypt()
{
	CryptSahara::closeSko(m_ctx, m_shwKey);
	CryptSahara::freeSymCtx(m_symCtx);
}

bool CryptSahara::SessionDecrypt::decrypt(unsigned char *result, const unsigned char * input, const size_t size,
		CallBackBase::Ptr callback)
{
	fsl_shw_return_t code;

	code = fsl_shw_symmetric_decrypt(m_ctx, m_shwKey, m_symCtx, size, input, result);
	if (code != FSL_RETURN_OK_S)
	{
		setErrorMsg("Could not decrypt: %s\n", fsl_error_string(code));
		return false;
	}

	if (size >= 16)
		memcpy(m_symCtx->context, input + size - 16, 16);

	if (callback)
		(*callback)();

	return true;
}

} /* namespace enginesound */

#endif /* SAHARA */



