/*
 * InterpolationHighLow.h
 *
 *  Created on: 15.09.2010
 *      Author: u4
 */

#ifndef ALGORITHM_INTERPOLATIONHIGHLOW_H_
#define ALGORITHM_INTERPOLATIONHIGHLOW_H_

#include <enginesound/algorithm/Interpolation.h>

namespace enginesound
{

template<class T_REGION, class T_VAL, class T_CALC>
class InterpolationHighLow
{
public:
	typedef std::pair<T_VAL, T_VAL> LowHigh;
	typedef typename std::map<T_REGION, LowHigh> Array;
	typedef typename Array::const_iterator ArrayConstIterator;

	typedef T_REGION region_type;
	typedef T_VAL value_type;
	typedef T_CALC calc_type;
public:
	InterpolationHighLow(const T_VAL lowMax = T_VAL(), const T_VAL highMax = T_VAL()) :
			m_arrayLowHigh(), m_valMax(lowMax, highMax)
	{
	}
	~InterpolationHighLow(void)
	{
	}

	void insert(const T_REGION key, const T_VAL Low, const T_VAL High)
	{
		m_arrayLowHigh[key] = LowHigh(Low, High);
	}

	void insert(const T_REGION key, const LowHigh & val)
	{
		m_arrayLowHigh[key] = val;
	}

	T_VAL operator()(const T_REGION key, const T_VAL val) const
	{
		if (m_arrayLowHigh.empty())
			return val;

		if (m_arrayLowHigh.size() == 1)
		{
			typename Array::const_iterator pos = m_arrayLowHigh.begin();
			const LowHigh & highlow = pos->second;
			const LinearInterpolation<T_VAL, T_VAL, T_CALC> calcObj(m_valMax.first, highlow.first, m_valMax.second,
					highlow.second, true);
			return calcObj(val);
		}

		T_REGION begin, end;
		LowHigh beginval, endval;

		std::pair<ArrayConstIterator, ArrayConstIterator> posLowerUpper = m_arrayLowHigh.equal_range(key);

		if (posLowerUpper.second == m_arrayLowHigh.end())
		{
			posLowerUpper.first--;
			posLowerUpper.second--;
		}

		if (posLowerUpper.first == posLowerUpper.second)
		{
			if (posLowerUpper.first == m_arrayLowHigh.begin())
				posLowerUpper.second++;
			else
				posLowerUpper.first--;

			begin = posLowerUpper.first->first;
			beginval = posLowerUpper.first->second;

		}
		else
		{
			begin = posLowerUpper.first->first;
			beginval = posLowerUpper.first->second;
		}

		end = posLowerUpper.second->first;
		endval = posLowerUpper.second->second;

		const LinearInterpolation<T_REGION, T_VAL, T_CALC> calcObjLow(begin, beginval.first, end, endval.first, true);
		const LinearInterpolation<T_REGION, T_VAL, T_CALC> calcObjHigh(begin, beginval.second, end, endval.second,
				true);

		const LinearInterpolation<T_VAL, T_VAL, T_CALC> calcObj(m_valMax.first, calcObjLow(key), m_valMax.second,
				calcObjHigh(key), true);
		return calcObj(val);
	}

	Array & getArray()
	{
		return m_arrayLowHigh;
	}
	const Array & getArray() const
	{
		return m_arrayLowHigh;
	}

private:
	Array m_arrayLowHigh;
	LowHigh m_valMax;
};

template<class T_REGION, class T_VAL, class T_CALC>
std::ostream & operator<<(std::ostream &stream, const InterpolationHighLow<T_REGION, T_VAL, T_CALC> obj)
{
	const typename InterpolationHighLow<T_REGION, T_VAL, T_CALC>::Array & points = obj.getArray();

	stream << "{ ";
	if (points.empty())
		stream << "NULL";
	else
		for (typename InterpolationHighLow<T_REGION, T_VAL, T_VAL>::Array::const_iterator pos = points.begin();
				pos != points.end(); ++pos)
		{
			if (pos != points.begin())
				stream << ", ";

			stream << "( " << pos->first << ": " << pos->second.first << ", " << pos->second.second << " )";
		}
	stream << "}";

	return stream;
}

}

#endif /* ALGORITHM_INTERPOLATIONHIGHLOW_H_ */

