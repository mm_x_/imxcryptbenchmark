/*
 * Inertia.h
 *
 *  Created on: 04.10.2010
 *      Author: user
 */

#ifndef INERTIA_H_
#define INERTIA_H_

namespace enginesound
{

class Inertia
{
public:
	Inertia(const float vol = 0, const float mass = 1) :
		m_cur(vol), m_next(vol), m_mass(mass)
	{
	}
	virtual ~Inertia()
	{
	}

	void set(const float vol)
	{
		m_next = vol;
	}

	float get(void) const
	{
		return m_cur;
	}

	void update(const float dTime)
	{
		m_cur += (m_next - m_cur) * m_mass * dTime;
	}

	void set_mass(const float mass)
	{
		m_mass = mass;
	}
private:
	float m_cur;
	float m_next;
	float m_mass;
};

}

#endif /* INERTIA_H_ */
