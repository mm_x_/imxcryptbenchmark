/*
 * check.h
 *
 *  Created on: 03.10.2012
 *      Author: u4
 */

#ifndef CHECK_H_
#define CHECK_H_

#include <stddef.h>

template<typename _T>
bool checkPeak(const _T * data, const size_t frames, const size_t channels, const double maxDiff)
{
	if (frames == 0)
		return true;

	if (channels == 0)
		return false;

	double * prevVal = new double[channels];
	for (size_t ch = 0; ch < channels; ch++)
		prevVal[ch] = data[ch];

	for (size_t frame = 1; frame < frames; frame++)
	{
		for (size_t ch = 0; ch < channels; ch++)
		{
			const double val = data[frame * channels + ch];
			const double maxVal = double(prevVal[ch]) + maxDiff;
			const double minVal = double(prevVal[ch]) - maxDiff;
			if (val >= minVal && val <= maxVal)
				prevVal[ch] = val;
			else
			{
				delete[] prevVal;
				return false;
			}
		}
	}

	delete[] prevVal;
	return true;
}

#endif /* CHECK_H_ */
