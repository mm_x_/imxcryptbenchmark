/*
 * Algorithm.h
 *
 *  Created on: 05.04.2010
 *      Author: user
 */

#ifndef ALGORITHM_H_
#define ALGORITHM_H_

#include <sstream>

template<class T_VAL>
T_VAL NoneNegative(const T_VAL val)
{
	return val < T_VAL(0) ? T_VAL(0) : val;
}

template<class T_VAL>
T_VAL strTo(const char * str)
{
	T_VAL retVal;

	std::stringstream stream;
	stream << str;

	stream >> retVal;

	return retVal;
}

template<class T_VAL>
T_VAL strTo(const char * str, const T_VAL & defVal)
{
	T_VAL retVal = defVal;

	std::stringstream stream;
	stream << str;

	stream >> retVal;

	return retVal;
}

template<class T_VAL>
T_VAL strHexTo(const char * str, const T_VAL & defVal)
{
	T_VAL retVal = defVal;

	std::stringstream stream;

	stream << str;

	stream >> std::hex >> retVal;

	return retVal;
}

template<class T_VAL>
T_VAL intervalToFreq(const T_VAL samplerate, const T_VAL frames)
{
	return samplerate / frames;
}

#endif /* ALGORITHM_H_ */
