/*
 * CalcPolicy
 *
 *  Created on: 17.03.2010
 *      Author: user
 */

#ifndef ESCALCPOLICY_H_
#define ESCALCPOLICY_H_

#include <enginesound/Macros.h>
#include "enginesound/algorithm/comparator/RegionPolicy.h"
#include "enginesound/algorithm/AvgBuff.h"

#include <boost/tr1/memory.hpp>
#include <iostream>
#include <iterator>

namespace enginesound
{
namespace comparator
{

template<typename _Iterator, typename _Calc>
class CalcPolicy
{
public:
	typedef typename std::iterator_traits<_Iterator>::difference_type
			difference_type;
	typedef _Calc value_type;

	typedef std::tr1::shared_ptr<CalcPolicy> Ptr;
public:
	CalcPolicy()
	{
		m_pPolicy = typename RegionPolicy<_Calc>::Ptr(
				new RegionPolicyMultAvg<_Calc> ());
	}
	CalcPolicy(typename RegionPolicy<_Calc>::Ptr pPolicy) :
		m_pPolicy(pPolicy)
	{

	}
	virtual ~CalcPolicy()
	{

	}

	virtual value_type getAvg() const = 0;

	virtual value_type getLow() const
	{
		return m_pPolicy->getLow(getAvg());
	}
	virtual value_type getHigh() const
	{
		return m_pPolicy->getHigh(getAvg());
	}

	virtual void calc(const _Iterator ES_UNUSED(begin),
			const _Iterator ES_UNUSED(end))
	{

	}

	virtual const char * name(void) const
	{
		return "unknown";
	}

	typename RegionPolicy<_Calc>::Ptr getPolicy()
	{
		return m_pPolicy;
	}

	void setPolicy(typename RegionPolicy<_Calc>::Ptr m_pPolicy)
	{
		this->m_pPolicy = m_pPolicy;
	}

	virtual void printStat(std::ostream & out)
	{
		out << name() << " h:" << getHigh() << " l:" << getLow() << " a:"
				<< getAvg() << ' ';
	}
private:
	typename RegionPolicy<_Calc>::Ptr m_pPolicy;
};

template<typename _Iterator, typename _Calc>
class CalcAvgFixedPolicy: public CalcPolicy<_Iterator, _Calc>
{
public:
	typedef typename CalcPolicy<_Iterator, _Calc>::value_type value_type;

	typedef std::tr1::shared_ptr<CalcAvgFixedPolicy> Ptr;
public:
	CalcAvgFixedPolicy() :
		CalcPolicy<_Iterator, _Calc> (), m_avg()
	{

	}
	CalcAvgFixedPolicy(typename RegionPolicy<_Calc>::Ptr pPolicy,
			value_type avg) :
		CalcPolicy<_Iterator, _Calc> (pPolicy), m_avg(avg)
	{

	}

	~CalcAvgFixedPolicy()
	{

	}

	virtual value_type getAvg() const
	{
		return m_avg;
	}

	void setAvg(const value_type m_avg)
	{
		this->m_avg = m_avg;
	}

	virtual const char * name(void) const
	{
		return "fixed";
	}
private:
	value_type m_avg;

};

template<typename _Iterator, typename _Calc>
class CalcAvgBuffPolicy: public CalcPolicy<_Iterator, _Calc>
{
public:
	typedef typename CalcPolicy<_Iterator, _Calc>::value_type value_type;
	typedef typename CalcPolicy<_Iterator, _Calc>::difference_type
			difference_type;

	typedef std::tr1::shared_ptr<CalcAvgBuffPolicy> Ptr;
public:
	CalcAvgBuffPolicy(const difference_type cap = 1024) :
		CalcPolicy<_Iterator, _Calc> (), m_buff(cap)
	{
	}
	CalcAvgBuffPolicy(typename RegionPolicy<_Calc>::Ptr pPolicy,
			const size_t cap = 1024) :
		CalcPolicy<_Iterator, _Calc> (pPolicy), m_buff(cap)
	{
	}

	~CalcAvgBuffPolicy()
	{

	}

	virtual value_type getAvg() const
	{
		return m_buff.get();
	}

	virtual void calc(const _Iterator begin, const _Iterator end)
	{
		for (_Iterator pos = begin; pos != end; ++pos)
			m_buff.push(*pos);
	}

	virtual const char * name(void) const
	{
		return "avg buffer";
	}
private:
	AvgBuff<_Iterator, _Calc> m_buff;
};

}
}

#endif /* ESCALCPOLICY_H_ */
