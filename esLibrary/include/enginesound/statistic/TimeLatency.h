/*
 * TimeLatency.h
 *
 *  Created on: 27.09.2010
 *      Author: user
 */

#ifndef TIMELATENCY_H_
#define TIMELATENCY_H_

#include <enginesound/system/Time.h>

#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>

#include <iostream>

namespace enginesound
{

class TimeLatency
{
public:
	TimeLatency() :
			m_mutex(), m_isFirst(true), m_timePrev(), m_cur(), m_min(), m_max(), m_summ(), m_nSumm()
	{

	}
	virtual ~TimeLatency()
	{

	}

	void clear()
	{
		boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lockDataSamples(m_mutex);

		m_min = Time();
		m_max = Time();
		m_summ = Time();
		m_nSumm = 0;
		m_isFirst = true;
	}

	void beginMeasure(void)
	{
		boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lockDataSamples(m_mutex);

		const Time time = Time::getCurrent();

		m_timePrev = time;
		m_isFirst = false;
	}

	void endMeasure(void)
	{
		boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lockDataSamples(m_mutex);

		const Time time = Time::getCurrent();

		if (m_isFirst)
		{
			m_isFirst = false;
		}
		else
		{
			m_cur = time - m_timePrev;

			m_max = std::max(m_cur, m_max);
			if (m_min == Time())
				m_min = m_cur;
			m_min = std::min(m_cur, m_min);
			m_summ += m_cur;
			++m_nSumm;
		}
	}

	void measure(void)
	{
		boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lockDataSamples(m_mutex);

		const Time time = Time::getCurrent();

		if (m_isFirst)
		{
			m_isFirst = false;
		}
		else
		{
			m_cur = time - m_timePrev;

			m_max = std::max(m_cur, m_max);
			if (m_min == Time())
				m_min = m_cur;
			m_min = std::min(m_cur, m_min);
			m_summ += m_cur;
			++m_nSumm;
		}

		m_timePrev = time;
	}

	bool isMeasure() const
	{
		return m_nSumm != 0;
	}

	Time getCur() const
	{
		return m_cur;
	}

	Time getMax() const
	{
		return m_max;
	}

	Time getMin() const
	{
		return m_min;
	}

	Time getAvg() const
	{
		if (m_nSumm == 0)
			return Time();

		return m_summ / m_nSumm;
	}
private:
	boost::interprocess::interprocess_mutex m_mutex;

	bool m_isFirst;
	Time m_timePrev;

	Time m_cur;
	Time m_min;
	Time m_max;
	Time m_summ;
	size_t m_nSumm;
};

std::ostream & operator<<(std::ostream &stream, const TimeLatency & obj);

}

#endif /* TIMELATENCY_H_ */
