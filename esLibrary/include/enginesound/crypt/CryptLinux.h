/*
 * CryptLinux.h
 *
 *  Created on: 29.10.2012
 *      Author: u4
 */

#ifndef CRYPTLINUX_H_
#define CRYPTLINUX_H_

#include <enginesound/crypt/CryptBase.h>

#ifdef DEVCRYPTOLINUX

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#include <sys/ioctl.h>
#include <linux/cryptodev.h>

namespace enginesound
{

class CryptLinux: public CryptBase
{
public:
	typedef std::tr1::shared_ptr<CryptLinux> Ptr;

	class SessionCrypt: public CryptBase::SessionCrypt
	{
		friend class CryptLinux;
	public:
		typedef std::tr1::shared_ptr<SessionCrypt> Ptr;

	public:
		virtual bool crypt(unsigned char *result, const unsigned char * input, const size_t size,
				CallBackBase::Ptr callback = CallBackBase::Ptr());
		~SessionCrypt();
	protected:
		SessionCrypt(int fd, const Algorithm alg, const Mode mode, const unsigned char * key, const size_t keysize);
	private:
		int m_fd;
		struct session_op* m_session;
		char * m_iv;
	};

	class SessionDecrypt: public CryptBase::SessionDecrypt
	{
		friend class CryptLinux;
	public:
		typedef std::tr1::shared_ptr<SessionDecrypt> Ptr;

	public:
		virtual bool decrypt(unsigned char *result, const unsigned char * input, const size_t size,
				CallBackBase::Ptr callback = CallBackBase::Ptr());
		~SessionDecrypt();
	protected:
		SessionDecrypt(int fd, const Algorithm alg, const Mode mode, const unsigned char * key, const size_t keysize);

	private:
		int m_fd;
		struct session_op* m_session;
		char * m_iv;
	};

public:
	CryptLinux();
	virtual ~CryptLinux();

	virtual bool init();
	virtual bool close();

	CryptBase::SessionCrypt::Ptr createCrypt(const Algorithm alg, const Mode mode, const unsigned char * key,
			const size_t keysize);
	CryptBase::SessionDecrypt::Ptr createDecrypt(const Algorithm alg, const Mode mode, const unsigned char * key,
			const size_t keysize);

	static struct session_op* setKey(int fd, const Algorithm alg, const Mode mode, const unsigned char * key,
			const size_t keysize);
	static void closeSession(int fd, struct session_op* session);
	static size_t getIVSize(const Algorithm alg, const Mode mode);
private:
	int m_fd;
};

} /* namespace enginesound */

#endif /* DEVCRYPTOLINUX */

#endif /* CRYPTLINUX_H_ */
