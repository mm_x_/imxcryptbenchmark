/*
 * Thread.h
 *
 *  Created on: 28.09.2010
 *      Author: user
 */

#ifndef THREAD_H_
#define THREAD_H_

#include <enginesound/system/PThread.h>
#include <enginesound/system/BoostThread.h>

namespace enginesound
{

#if !defined (_MSC_VER) && !defined (__MINGW32__)

#if defined (_ES_BOOST_THREAD_)
typedef BoostThread Thread;
#else
typedef PThread Thread;
#endif

#else
typedef BoostThread Thread;
#endif

void msleep(const size_t msec);

}

#endif /* THREAD_H_ */
