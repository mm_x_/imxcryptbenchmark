/*
 * LogStd.h
 *
 *  Created on: 19.07.2010
 *      Author: user2
 */

#ifndef LOGSTD_H_
#define LOGSTD_H_

#include <enginesound/log/LogBase.h>
#include <stdio.h>

namespace enginesound
{

class LogStd: public LogBase
{
public:
	LogStd(const size_t minLevel = 0);
	virtual ~LogStd();

	bool is_ok() const
	{
		return false;
	}

	virtual void log(const size_t level, const char * msg);
private:
	LogStd(const enginesound::LogStd&);
	void operator=(const enginesound::LogStd&);
};

}

#endif /* LOGSTD_H_ */
