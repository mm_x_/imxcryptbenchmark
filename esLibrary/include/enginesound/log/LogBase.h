/*
 * LogBase.h
 *
 *  Created on: 27.01.2010
 *      Author: user
 */

#ifndef LOGBASE_H_
#define LOGBASE_H_

#include <enginesound/system/Time.h>
#include <enginesound/core/ErrorObject.h>

#include <string>
#include <ctime>

#include <boost/tr1/memory.hpp>

namespace enginesound
{

class LogBase: public ErrorObject
{
public:
	typedef ::std::tr1::shared_ptr<LogBase> Ptr;

public:
	LogBase(const size_t minLevel = 0);
	virtual ~LogBase();

	virtual void close()
	{

	}

	virtual void log(const size_t level, const char * msg);
	virtual bool is_ok() const = 0;

	void setMinLevel(const size_t level)
	{
		m_minLevel = level;
	}
	size_t getMinLevel(void) const
	{
		return m_minLevel;
	}
protected:
	inline const char * levelToStr(const size_t level)
	{
		const char * info = "info";
		const char * warning = "warning";
		const char * error = "error";
		const char * critical_error = "critical error";
		const char * unknown = "unknown";

		switch (level)
		{
		case 0:
			return info;
		case 1:
			return warning;
		case 2:
			return error;
		case 3:
			return critical_error;
		default:
			return unknown;
		};

		return unknown;
	}

private:
	static Time m_timeStart;
	size_t m_minLevel;
};

}

#endif /* LOGBASE_H_ */
