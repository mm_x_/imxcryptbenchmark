/*
 * LogFile.h
 *
 *  Created on: 27.01.2010
 *      Author: user
 */

#ifndef LOGFILE_H_
#define LOGFILE_H_

#include <iostream>
#include <fstream>

#include <enginesound/log/LogBase.h>

namespace enginesound
{

class LogFile: public enginesound::LogBase
{
public:
	typedef std::tr1::shared_ptr<LogFile> Ptr;
	static const char * defaultPath;
public:
	LogFile(const char * filename = defaultPath, const size_t minLevel = 0);
	virtual ~LogFile();

	bool init(const char * filename = defaultPath);
	void close();

	bool is_ok() const
	{
		return m_logFile.is_open();
	}

	virtual void log(const size_t level, const char * msg)
	{
		if (!is_ok())
			return;

		LogBase::log(level, msg);

		if (level >= getMinLevel())
		{
			m_logFile << getUpTime() << ' ' << levelToStr(level) << ": ";
			m_logFile << msg << std::endl;
		}
	}

private:
	std::ofstream m_logFile;
};

}

#endif /* LOGFILE_H_ */
