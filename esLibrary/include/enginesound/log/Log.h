/*
 * Log.h
 *
 *  Created on: 19.07.2010
 *      Author: user2
 */

#ifndef LOG_H_
#define LOG_H_

#include <boost/thread/thread.hpp>

#include <cstdarg>
#include <list>

#include <enginesound/log/LogBase.h>
#include <string>

namespace enginesound
{

::std::string vmessageToString(const char * format, ...);
::std::string vmessageToString(const char * format, va_list arg);

class Log
{
public:
	typedef ::std::list<LogBase::Ptr> arrayLogObject;

	/*
	 log level:
	 0 - info
	 1 - warning
	 2 - error
	 3 - critical error
	 4 - message
	 */
public:
	Log();
	virtual ~Log();

	static void info(const char * format, ...);
	static void warning(const char * format, ...);
	static void error(const char * format, ...);
	static void critical_error(const char * format, ...);
	static void message(const char * format, ...);
	static void vlog(const int level, const char * format, va_list arg);

	static void log(const int level, const char * format, ...);

	static void addLog(LogBase::Ptr obj);
	static void delLog(LogBase::Ptr obj);
	static void clearLogList();
	static arrayLogObject & getLogObjList();

	static void setLevel(const int level);
	static int getLevel(void);
protected:
	static int & getLevelLocal(void);
};

}

#endif /* LOG_H_ */
