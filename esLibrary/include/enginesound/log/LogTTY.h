/*
 * LogTTY.h
 *
 *  Created on: 21.11.2011
 *      Author: user
 */

#ifndef LOGTTY_H_
#define LOGTTY_H_

#include <enginesound/log/LogBase.h>

#include <boost/asio.hpp>
#include <boost/bind.hpp>

namespace enginesound
{

class LogTTY: public enginesound::LogBase
{
public:
	typedef std::tr1::shared_ptr<LogTTY> Ptr;

	static const char * defaultPath;
public:
	LogTTY(const char * filename = defaultPath, const size_t baud = 115200, const size_t minLevel = 0);
	virtual ~LogTTY();

	bool init(const char * filename = defaultPath, const size_t baud = 115200);
	void close();
	bool is_ok() const
	{
		if (!m_pPort)
			return false;
		return m_pPort->is_open();
	}

	virtual void log(const size_t level, const char * msg);
private:
	LogTTY(const enginesound::LogTTY&);
	LogTTY operator = (const enginesound::LogTTY&);

	boost::asio::io_service * m_pService;
	boost::asio::serial_port * m_pPort;
};

} /* namespace enginesound */
#endif /* LOGTTY_H_ */
